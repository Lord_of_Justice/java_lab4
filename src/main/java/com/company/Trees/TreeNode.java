package com.company.Trees;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TreeNode<T> implements Iterable<TreeNode<T>> {
    private T data;
    private TreeNode<T> parent;
    private List<TreeNode<T>> children;
    /** Чорний колір вузла. */
    public static final boolean BLACK = false;
    /** Червоний колір вузла. */
    public static final boolean RED = true;
    public boolean color;

    public TreeNode(T data){
        this.data = data;
        parent = null;
        children = new ArrayList<>();
        color = BLACK;
    }

    @Override
    public Iterator<TreeNode<T>> iterator() {
        return null;
    }

    public void addChild(TreeNode<T> node){
        if (node != null){
            node.setParent(this);
        }
        children.add(node);
    }
    public void addChildren(List<TreeNode<T>> nodes){
        for (TreeNode node: nodes) {
            addChild(node);
        }
    }
    public void removeChildren(){
        children = new ArrayList<>();
    }
    public boolean isEmptyChildren(){
        return children.isEmpty();
    }
    public T getData(){
        return data;
    }
    public TreeNode<T> getParent(){
        return parent;
    }
    public List<TreeNode<T>> getChildren() {
        return children;
    }
    public TreeNode<T> getChild(int index){
        if (children.isEmpty()) {
            System.out.println("Error: Node has no one child. Fill it at first.");
            return null;
        }
        if (index >= 0 && index < children.size()){
            return children.get(index);
        }
        System.out.println("Error: There is no child with index " + index);
        return null;
    }
    public void setChild(int index, TreeNode<T> value){
        children.set(index, value);
    }
    public void setData(T data) {
        this.data = data;
    }
    public void setParent(TreeNode<T> node){
        this.parent = node;
    }
    public BST_Side getNodeSide() {
        return parent == null
                ? BST_Side.Root
                : parent.getChild(0) == this
                ? BST_Side.Left
                : BST_Side.Right;
    }
}

