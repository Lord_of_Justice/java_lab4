package com.company.Trees;

public class BinarySearchTree<T extends Comparable<T>>{
    // left - 0 дитина
    // right - 1 дитина
    public TreeNode<T> root;
    public int count;

    public BinarySearchTree(){
        root = null;
        count = 0;
    }

    public void add(T data){
        TreeNode<T> node = new TreeNode<>(data);
        node.addChild(null);
        node.addChild(null);
        add(node, null);
        node.color = TreeNode.RED;
        addFixUp(node);
    }
    private void add(TreeNode<T> node, TreeNode<T> currentNode){
        if(root == null) {
            node.setParent(null);
            root = node;
            count++;
        }
        else {
            currentNode = currentNode == null ? root : currentNode;
            node.setParent(currentNode);
            int result = node.getData().compareTo(currentNode.getData());
            if (result == 0){
                currentNode.setData(node.getData());
            }
            else if (result < 0){
                if (currentNode.getChild(0) == null){
                    currentNode.setChild(0, node);
                    count++;
                }
                else {
                    add(node, currentNode.getChild(0));
                }
            }
            else {
                if (currentNode.getChild(1) == null){
                    currentNode.setChild(1, node);
                    count++;
                }
                else {
                    add(node, currentNode.getChild(1));
                }
            }
        }
    }
    private void addFixUp(TreeNode<T> startNode){
        if (startNode.getParent() == null) {
            startNode.color = TreeNode.BLACK;
        } else if (startNode.getParent().color == TreeNode.RED){
            TreeNode<T> parent = startNode.getParent();
            TreeNode<T> grandParent = parent.getParent();
            if (parent.getNodeSide() == BST_Side.Left){
                TreeNode<T> aunt = grandParent.getChild(1);
                if (aunt != null && aunt.color == TreeNode.RED){
                    recoloring(parent, aunt, grandParent);
                }
                else {
                    if (startNode.getNodeSide() == BST_Side.Right) {
                        rotateLeft(parent);
                    }
                    grandParent.getChild(0).color = TreeNode.BLACK;
                    grandParent.color = TreeNode.RED;
                    rotateRight(grandParent);
                }
            }
            else {
                TreeNode<T> aunt = grandParent.getChild(0);
                if (aunt != null && aunt.color == TreeNode.RED){
                    recoloring(parent, aunt, grandParent);
                }
                else {
                    if (startNode.getNodeSide() == BST_Side.Left) {
                        rotateRight(parent);
                    }
                    grandParent.getChild(1).color = TreeNode.BLACK;
                    grandParent.color = TreeNode.RED;
                    rotateLeft(grandParent);
                }
            }
        }
    }

    public void remove(T data){
        try{
            TreeNode<T> node = findNode(data);
            remove(node);
            removeFixDoubleBlack(node);
        } catch (Exception e){
            System.out.println(e.toString());
        }
    }
    private void remove(TreeNode<T> node) throws Exception {
        if (node == null){
            throw new Exception("Node is null, so it`s impossible to delete!");
        }
        else {
            BST_Side currentNodeSide = node.getNodeSide();
            if (node.getChild(0) == null && node.getChild(1) == null) {
                removeNodeWithoutChildren(node, currentNodeSide);

            }
            else if (node.getChild(0) == null) {
                removeNodeWithRightChild(node, currentNodeSide);
            }
            else if (node.getChild(1) == null) {
                removeNodeWithLeftChild(node, currentNodeSide);
            }
            else {
                TreeNode<T> minNode = GetMinimum(node.getChild(1));
                node.setData(minNode.getData());
                remove(minNode);
            }
        }
    }
    private void removeNodeWithRightChild(TreeNode<T> node, BST_Side currentNodeSide){
        if (currentNodeSide == BST_Side.Root){
            root = node.getChild(1);
            node.setChild(1, null);
        } else {
            if (currentNodeSide == BST_Side.Left) {
                node.getParent().setChild(0, node.getChild(1));
            }
            else {
                node.getParent().setChild(1, node.getChild(1));
            }
            node.getChild(1).setParent(node.getParent());
        }
        count--;
    }
    private void removeNodeWithLeftChild(TreeNode<T> node, BST_Side currentNodeSide){
        if (currentNodeSide == BST_Side.Root){
            root = node.getChild(0);
            node.setChild(1, null);
        } else {
            if (currentNodeSide == BST_Side.Left) {
                node.getParent().setChild(0, node.getChild(0));
            }
            else {
                node.getParent().setChild(1, node.getChild(0));
            }
            node.getChild(0).setParent(node.getParent());
        }
        count--;
    }
    private void removeNodeWithoutChildren(TreeNode<T> node, BST_Side currentNodeSide){
        if (currentNodeSide == BST_Side.Root) {
            root = null;
        }
        else if (currentNodeSide == BST_Side.Right){
            node.getParent().setChild(1, null);
        }
        else {
            node.getParent().setChild(0, null);
        }
        count--;
    }
    private void removeFixDoubleBlack(TreeNode<T> delete){
        TreeNode<T> sibling = delete.getParent().getChild(0);
        if (sibling != null ){
            sibling = delete.getParent().getChild(1);
        }
        BST_Side siblingSide = sibling.getNodeSide();
        if (delete.color == TreeNode.RED
                || (delete.getChild(0) != null && sibling.getChild(0).color == TreeNode.RED)
                || (delete.getChild(1) != null && sibling.getChild(1).color == TreeNode.RED)){
            if (delete.getChild(0) != null){
                delete.getChild(0).color = TreeNode.BLACK;
            } else if (delete.getChild(1) != null){
                delete.getChild(1).color = TreeNode.BLACK;
            }
        }
        else if (sibling.color == TreeNode.BLACK){
            if (siblingSide == BST_Side.Left){
                if (sibling.getChild(0) != null && sibling.getChild(0).color == TreeNode.RED){
                    sibling.getChild(0).color = TreeNode.BLACK;
                    rotateRight(sibling.getParent());
                }
                else if (sibling.getChild(1) != null && sibling.getChild(1).color == TreeNode.RED) {
                    sibling.getChild(1).color = TreeNode.BLACK;
                    rotateLeft(sibling);
                    rotateRight(sibling.getParent().getParent());
                }
                else {
                    sibling.color = TreeNode.RED;
                    if (sibling.getParent().color = TreeNode.BLACK){
                        removeFixDoubleBlack(sibling.getParent());
                    } else {
                        sibling.getParent().color = TreeNode.BLACK;
                    }
                }
            }
            else {
                if (sibling.getChild(1) != null && sibling.getChild(1).color == TreeNode.RED){
                    sibling.getChild(1).color = TreeNode.BLACK;
                    rotateLeft(sibling.getParent());
                }
                else if (sibling.getChild(0) != null && sibling.getChild(0).color == TreeNode.RED) {
                    sibling.getChild(0).color = TreeNode.BLACK;
                    rotateRight(sibling);
                    rotateLeft(sibling.getParent().getParent());
                }
                else {
                    sibling.color = TreeNode.RED;
                    if (sibling.getParent().color = TreeNode.BLACK){
                        removeFixDoubleBlack(sibling.getParent());
                    } else {
                        sibling.getParent().color = TreeNode.BLACK;
                    }
                }
            }
        } else {
            sibling.color = TreeNode.BLACK;
            sibling.getParent().color = TreeNode.RED;
            TreeNode<T> parent = sibling.getParent();
            if (siblingSide == BST_Side.Left){
                rotateRight(sibling);
                removeFixDoubleBlack(parent.getChild(0));
            }
            else {
                rotateLeft(sibling);
                removeFixDoubleBlack(parent.getChild(1));
            }
        }

    }

    private TreeNode<T> GetMinimum(TreeNode<T> node) {
        while (node.getChild(0) != null){
            node = node.getChild(0);
        }
        return node;
    }

    public void clear(){
        root = null;
        count = 0;
    }
    public boolean contains(T data){
        return findNode(data, null) != null ? true : false;
    }
    public T findValue(T data) {
        return findNode(data).getData();
    }
    public TreeNode<T> findNode(T data){
        return findNode(data, null);
    }
    private TreeNode<T> findNode(T data, TreeNode<T> startWithNode){
        startWithNode = startWithNode == null ? root : startWithNode;
        if (startWithNode != null) {
            int result = data.compareTo(startWithNode.getData());
            if (result == 0){
                return startWithNode;
            }
            else if (result < 0 && startWithNode.getChild(0) != null){
                return findNode(data, startWithNode.getChild(0));
            }
            else if (startWithNode.getChild(1) != null){
                return findNode(data, startWithNode.getChild(1));
            }
        }
        return null;
    }


    private void recoloring(TreeNode<T> parent, TreeNode<T> aunt, TreeNode<T> grandpa){
        parent.color = TreeNode.BLACK;
        aunt.color = TreeNode.BLACK;
        grandpa.color = TreeNode.RED;
        addFixUp(grandpa);
    }
    private void rotateLeft(TreeNode<T> rotate){
        TreeNode<T> parentRotate = rotate.getParent();
        TreeNode<T> rotateChild = rotate.getChild(1);
        rotate.setChild(1, rotateChild.getChild(0));
        if (rotateChild.getChild(0) != null) {
            rotateChild.getChild(0).setParent(rotate);
        }
        rotateChild.setParent(parentRotate);
        if (parentRotate == null){
            root = rotateChild;
        } else if (rotate.getNodeSide() == BST_Side.Left){
            parentRotate.setChild(0, rotateChild);
        } else {
            parentRotate.setChild(1, rotateChild);
        }
        rotateChild.setChild(0, rotate);
        rotate.setParent(rotateChild);
    }
    private void rotateRight(TreeNode<T> rotate){
        TreeNode<T> parentRotate = rotate.getParent();
        TreeNode<T> rotateChild = rotate.getChild(0);
        rotate.setChild(0, rotateChild.getChild(1));
        if (rotateChild.getChild(1) != null) {
            rotateChild.getChild(1).setParent(rotate);
        }
        rotateChild.setParent(parentRotate);
        if (parentRotate == null){
            root = rotateChild;
        } else if (rotate.getNodeSide() == BST_Side.Left){
            parentRotate.setChild(0, rotateChild);
        } else {
            parentRotate.setChild(1, rotateChild);
        }
        rotateChild.setChild(1, rotate);
        rotate.setParent(rotateChild);
    }
}
