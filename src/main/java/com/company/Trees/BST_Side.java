package com.company.Trees;

public enum BST_Side {
    Root,
    Left,
    Right
}
