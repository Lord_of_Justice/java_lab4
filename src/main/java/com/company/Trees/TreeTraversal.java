package com.company.Trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TreeTraversal {
    public static<T> List<T> DepthFirstTraversal(TreeNode<T> root) {
        if(root != null){
            List<T> values = new ArrayList<>();
            List<TreeNode<T>> children =  root.getChildren();
            for(TreeNode<T> element : children){
                if (element != null) {
                    for (T data : DepthFirstTraversal(element)) {
                        values.add(data);
                    }
                }
            }
            values.add(root.getData());
            return values;
        }
        return null;
    }

    public static<T> List<T> BreadthFirstTraversal(TreeNode<T> root) {
        if(root != null){
            List<T> values = new ArrayList<>();
            Queue<TreeNode<T>> queue = new LinkedList<>();
            queue.add(root);
            while (!queue.isEmpty()){
                int children = queue.size();
                while (children > 0) {
                    TreeNode<T> child = queue.remove();
                    values.add(child.getData());
                    for (TreeNode<T> node : child.getChildren()){
                        if (node != null) {
                            queue.add(node);
                        }
                    }
                    children--;
                }
            }
            return values;
        }
        return null;
    }

}
