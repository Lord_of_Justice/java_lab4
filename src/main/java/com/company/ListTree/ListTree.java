package com.company.ListTree;

import com.company.List.ListOnTreeNode;
import com.company.Trees.BinarySearchTree;
import com.company.Trees.TreeTraversal;

import java.util.List;

public class ListTree<K extends Comparable, V> {
    int M = 8;
    int size = 0;
    public ListOnTreeNode<Pair<K,V>> list;
    public BinarySearchTree<Pair<K,V>> tree;
    public ListTree(){
        list = new ListOnTreeNode<>();
        tree = new BinarySearchTree<>();
    }
    //This class should have the following methods:
    public void put(K k, V v) {
        Pair<K, V> pair = new Pair<>(k, v);
        if (size + 1 < M) {
            list.add(pair);
        } else {
            if (tree.root == null) {
                treeify();
            }
            tree.add(pair);
        }
        size++;
    }
    public V get(K k) {
        Pair<K,V> pair;
        if (size < M){
            pair = list.get(new Pair<>(k, null));
        }
        else {
            pair = tree.findValue(new Pair<>(k, null));
        }
        return pair.getValue();
    }
    public boolean isEmpty() {
        return (list.isEmpty() && tree.count == 0);
    }
    public void remove(K k) {
        Pair<K, V> pair = new Pair<>(k, null);
        if (size < M) {
            list.remove(pair);
        } else {
            tree.remove(pair);
            if (size - 1 < 8) {
                listify();
            }
        }
        size--;
    }
    public boolean contains(K k) {
        return size < M ? list.contains(new Pair<>(k, null)) : tree.contains(new Pair<>(k, null));
    }
    public long size() {
        return size;
    }

    public void listify() {
        list = new ListOnTreeNode<>();
        List<Pair<K,V>> listOfValues = TreeTraversal.DepthFirstTraversal(tree.root);
        for (Pair<K,V> pair : listOfValues){
            list.add(pair);
        }
        tree.clear();
    }
    public void treeify() {
        tree = new BinarySearchTree<>();
        for (int i = 0; i < list.size(); i++){
            tree.add(list.get(i));
        }
        list.clear();
    }
}
