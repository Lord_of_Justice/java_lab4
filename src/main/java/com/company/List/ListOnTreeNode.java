package com.company.List;

import com.company.Trees.TreeNode;

public class ListOnTreeNode<T extends Comparable<T>> {
    private TreeNode<T> head;
    private long size;
    public ListOnTreeNode(){
        head = null;
        size = 0;
    }

    public boolean isEmpty(){
        return head == null;
    }
    public boolean contains(T data){
        TreeNode<T> curr = head;
        while(curr != null && curr.getData().compareTo(data) != 0){
            if(!curr.isEmptyChildren()){
                curr = curr.getChild(0);
            } else {
                curr = null;
            }
        }
        return curr == null ? false : true;
    }
    public long size(){
        return size;
    }
    public void add(T data){
        TreeNode<T> node = new TreeNode<>(data);
        if (head == null) {
            head = node;
        } else {
            TreeNode<T> curr = head;
            while (!curr.isEmptyChildren()){
                curr = curr.getChild(0);
            }
            curr.addChild(node);
        }
        size++;
    }
    public void remove(T data){
        if (head != null) {
            TreeNode<T> needToRemove = head;
            TreeNode<T> prev = null;
            while(needToRemove != null && !needToRemove.getData().equals(data)){
                prev = needToRemove;
                if(!needToRemove.isEmptyChildren()){
                    needToRemove = needToRemove.getChild(0);
                } else {
                    needToRemove = null;
                }
            }
            if(needToRemove != null) {
                if (prev == null) {
                    if (!head.isEmptyChildren()) {
                        head = head.getChild(0);
                    }
                    else {
                        head = null;
                    }
                } else {
                    TreeNode<T> del = prev.getChild(0);
                    prev.removeChildren();
                    if (!del.isEmptyChildren()){
                        prev.addChild(del.getChild(0));
                    }
                }
                size--;
            }
        }
    }
    public void clear(){
        head = null;
        size = 0;
    }
    public T get(T data){
        TreeNode<T> node = head;
        while (node != null){
            if (data.compareTo(node.getData()) == 0) {
                return node.getData();
            }
            if(!node.isEmptyChildren()){
                node = node.getChild(0);
            }
            else {
                node = null;
                break;
            }
        }
        return node == null ? null: node.getData();
    }
    public T get(int index){
        int counter = 0;
        TreeNode<T> node = head;
        while (counter < index){
            if(!node.isEmptyChildren()){
                node = node.getChild(0);
            }
            else {
                node = null;
                break;
            }
            counter++;
        }
        return node == null ? null: node.getData();
    }
}
