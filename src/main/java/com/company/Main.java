package com.company;

import com.company.List.ListOnTreeNode;
import com.company.ListTree.ListTree;
import com.company.ListTree.Pair;
import com.company.Trees.BinarySearchTree;
import com.company.Trees.TreeNode;
import com.company.Trees.TreeTraversal;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        /*TreeNode<Integer> root = new TreeNode<>(1);
        TreeNode<Integer> n2 = new TreeNode<>(2);
        TreeNode<Integer> n3 = new TreeNode<>(3);
        TreeNode<Integer> n4 = new TreeNode<>(4);
        TreeNode<Integer> n5 = new TreeNode<>(5);
        TreeNode<Integer> n7 = new TreeNode<>(7);
        root.addChild(n2);
        root.addChild(n3);
        root.addChild(n4);
        root.addChild(n5);
        n2.addChild(new TreeNode<>(6));
        n2.addChild(n7);
        n4.addChild(new TreeNode<>(8));
        n7.addChild(new TreeNode<>(9));
        n7.addChild(new TreeNode<>(10));
        n7.addChild(new TreeNode<>(11));

        List<Integer> list = TreeTraversal.BreadthFirstTraversal(root);
        for(int el : list){
            System.out.print(el + ", ");
        }*/
        ListTree<Integer, String> lt = new ListTree<>();
        String[] numbers = new String[]
                {"one", "two", "three", "four", "five",
                        "six","seven"};/*, "eight", "nine", "ten",
                 "eleven", "twelve", "thirteen", "fourteen"};*/
        int counter = 1;
        for(String number: numbers){
            lt.put(counter, number);
            counter++;
        }
        lt.put(9, "nine");

        List<Pair<Integer, String>> l = TreeTraversal.DepthFirstTraversal(lt.tree.root);
        for (Pair<Integer, String> pair: l){
            System.out.print(pair.getValue() + ", ");
        }
        System.out.println("\n==================================");
        l = TreeTraversal.BreadthFirstTraversal(lt.tree.root);
        for (Pair<Integer, String> pair : l){
            System.out.print(pair.getValue() + ", ");
        }
    }
}
