import com.company.List.ListOnTreeNode;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.*;
import java.util.List;

public class Test_ListOnTreeNode {
    ListOnTreeNode<Integer> list;
    @Before
    public void init(){
        list = new ListOnTreeNode<>();
        list.add(1);
        list.add(2);
        list.add(50);
        list.add(-6);
        list.add(10);
        list.add(9);
        list.add(-35);
    }
    @Test
    public void get_ElementInList_Element(){
        // arrange
        int expectedValue = 10;
        // act
        int el = list.get(expectedValue);
        // assert
        assertTrue(list.contains(el));
        assertEquals(expectedValue, el);
    }
    @Test
    public void get_ElementIsNotInList_Element(){
        // arrange
        int expectedValue = 100;
        // act
        Object el = list.get(expectedValue);
        // assert
        assertNull(el);
    }
    @Test
    public void clear_ListWithElements_NullList(){
        // arrange
        // act
        list.clear();
        // assert
        assertEquals(0, list.size());
    }
    @Test
    public void remove_ElementInList_Removed(){
        // arrange
        int expectedValue1 = -6;
        int expectedValue2 = -35;
        // act
        list.remove(expectedValue1);
        list.remove(expectedValue2);
        // assert
        assertFalse(list.contains(expectedValue1));
        assertFalse(list.contains(expectedValue2));
        assertEquals(5, list.size());
    }
}
