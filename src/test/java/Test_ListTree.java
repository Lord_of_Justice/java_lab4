import com.company.ListTree.ListTree;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.*;

public class Test_ListTree {

    ListTree<Integer, String> lt;
    String[] numbers;
    @Before
    public void init(){
        lt = new ListTree<>();
        numbers = new String[]
                {"one", "two", "three", "four", "five",
                 "six","seven"};/*, "eight", "nine", "ten",
                 "eleven", "twelve", "thirteen", "fourteen"};*/
        int counter = 1;
        for(String number: numbers){
            lt.put(counter, number);
            counter++;
        }
    }

    @Test
    public void get_ElementWithKey_Element() {
        // arrange
        int key = 5;
        // act
        String result = lt.get(key);
        // assert
        assertNotNull(result);
        assertEquals(result, "five");
    }
    @Test
    public void isEmpty_ListTree_TrueAndFalse() {
        // arrange
        ListTree<Integer, String> ltempty = new ListTree<>();
        // act
        boolean res1 = ltempty.isEmpty();
        boolean res2 = lt.isEmpty();
        // assert
        assertTrue(res1);
        assertFalse(res2);
    }
    @Test
    public void remove_ElementWithKey_ElementRemoved() {
        // arrange
        int key = 7;
        // act
        String result = lt.get(key);
        // assert
        assertNotNull(result);
        assertEquals(result, "seven");
    }
    @Test
    public void contains_ElementWithKey_TrueAndFalse() {
        // arrange
        int key = 6;
        int key2 = 50;
        // act
        boolean res1 = lt.contains(key);
        boolean res2 = lt.contains(key2);
        // assert
        assertTrue(res1);
        assertFalse(res2);
    }
    @Test
    public void size_ListTree_SizeOfListTree() {
        // arrange
        ListTree<Integer, String> ltempty = new ListTree<>();
        // act
        long res1 = ltempty.size();
        long res2 = lt.size();
        // assert
        assertEquals(0, res1);
        assertEquals(7, res2);
    }
}
