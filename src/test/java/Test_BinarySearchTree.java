import com.company.Trees.BinarySearchTree;
import com.company.Trees.TreeNode;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.*;

public class Test_BinarySearchTree {

    BinarySearchTree<Integer> TestTree;
    @Before
    public void init(){
        TestTree = new BinarySearchTree<>();
        TestTree.add(10);
        TestTree.add(7);
        TestTree.add(12);
        TestTree.add(9);
        TestTree.add(5);
        TestTree.add(3);
        TestTree.add(16);
        TestTree.add(15);
        TestTree.add(17);
    }

    @Test
    public void Contains_EntityInTree_IsInTree() {
        // arrange
        // act
        boolean containsTest2 = TestTree.contains(12);
        // assert
        assertTrue(containsTest2);
    }
    @Test
    public void Remove_ElementWithoutChildren_RemovedElement()
    {
        // arrange
        int count = TestTree.count;
        // act
        TestTree.remove(17);
        TestTree.remove(15);
        // assert
        assertFalse(TestTree.contains(17));
        assertFalse(TestTree.contains(15));
        assertEquals(count - 2, TestTree.count);
    }
    @Test
    public void Remove_ElementWithLeftChild_RemovedWithChangesInTree()
    {
        // arrange
        int count = TestTree.count;
        TreeNode<Integer> removedNode = TestTree.findNode(5);
        // act
        TestTree.remove(5);
        // assert
        assertFalse(TestTree.contains(5));
        assertSame(removedNode.getChild(0), removedNode.getParent().getChild(0));
        assertEquals(count - 1, TestTree.count);
    }
    @Test
    public void Remove_ElementWithRightChild_RemovedWithChangesInTree()
    {
        // arrange
        int count = TestTree.count;
        TreeNode<Integer> removedNode = TestTree.findNode(12);
        // act
        TestTree.remove(12);
        // assert
        assertFalse(TestTree.contains(12));
        assertSame(removedNode.getChild(1), removedNode.getParent().getChild(1));
        assertEquals(count - 1, TestTree.count);
    }
    @Test
    public void Remove_ElementWithChildren_RemovedWithChangesInTree()
    {
        // arrange
        int count = TestTree.count;
        TreeNode<Integer> removedNode = TestTree.findNode(7);
        // act
        TestTree.remove(7);
        // assert
        assertFalse(TestTree.contains(7));
        assertEquals(9, (int)removedNode.getParent().getChild(0).getData());
        assertEquals(count - 1, TestTree.count);
    }
    @Test
    public void Remove_RootWithChildren_TreeWithNewRoot()
    {
        // arange
        TreeNode<Integer> removedRoot = TestTree.findNode(10);
        // act
        TestTree.remove(10);
        // assert
        assertEquals(0, TestTree.root.getData().compareTo(12));
        assertSame(TestTree.root.getChild(0), removedRoot.getChild(0));
    }
    @Test
    public void Remove_RootWithoutChildren_NullTree()
    {
        // arrange
        TestTree.root.setChild(0, null);
        TestTree.root.setChild(1, null);
        // act
        TestTree.remove(10);
        // assert
        assertNull(TestTree.root);
    }
}
