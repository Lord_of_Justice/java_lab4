import com.company.Trees.TreeNode;
import com.company.Trees.TreeTraversal;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.*;

public class Test_TreeNode<T> {
    TreeNode<Integer> root;
    TreeNode<Integer> n2;
    TreeNode<Integer> n3;
    TreeNode<Integer> n4;
    TreeNode<Integer> n5;

    // tree:    1
    //    2   3   4     5
    //   6 7      8   (9 10 11) - будуть додані потім
    @Before
    public void init(){
        root = new TreeNode<>(1);
        n2 = new TreeNode<>(2);
        n3 = new TreeNode<>(3);
        n4 = new TreeNode<>(4);
        n5 = new TreeNode<>(5);
        root.addChild(n2);
        root.addChild(n3);
        root.addChild(n4);
        root.addChild(n5);
        n2.addChild(new TreeNode<>(6));
        n2.addChild(new TreeNode<>(7));
        n4.addChild(new TreeNode<>(8));
    }

    @Test
    public void addChildren_ThreeChildren_TreeWithFourNodes() {
        // arrange
        List<TreeNode<Integer>> list = new ArrayList<>();
        TreeNode<Integer> n9 = new TreeNode<>(9);
        TreeNode<Integer> n10 = new TreeNode<>(10);
        list.add(n9);
        list.add(n10);
        list.add(new TreeNode<>(11));
        // act
        n5.addChildren(list);
        // assert
        assertEquals(n5.getChild(0), n9);
        assertEquals(n5.getChild(1), n10);
    }
    @Test
    public void getParent_Child_HisParent() {
        // arrange
        // act
        TreeNode<Integer> parent = n3.getParent();
        // assert
        assertEquals(parent, root);
    }
    @Test
    public void getChild_Parent_HisChildren() {
        // arrange
        // act
        TreeNode<Integer> child1 = root.getChild(0);
        TreeNode<Integer> child2 = root.getChild(1);
        // assert
        assertEquals(child1, n2);
        assertEquals(child2, n3);
    }
    @Test
    public void DepthFirstTraversal_Tree_StringWithValues() {
        // arrange
        // in-order
        String expectedResult = "6, 7, 2, 3, 8, 4, 5, 1, ";
        // act
        List<Integer> list = TreeTraversal.DepthFirstTraversal(root);
        String result = "";
        for (int el: list) {
            result = result.concat(el + ", ");
        }
        // assert
        assertEquals(expectedResult, result);
    }
    @Test
    public void BreadthFirstTraversal_Tree_StringWithValues() {
        // arrange
        String expectedResult = "1, 2, 3, 4, 5, 6, 7, 8, ";
        // act
        List<Integer> list = TreeTraversal.BreadthFirstTraversal(root);
        String result = "";
        for (int el : list) {
            result = result.concat(el + ", ");
        }
        // assert
        assertEquals(expectedResult, result);
    }
}